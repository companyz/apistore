var request = require('supertest');
var should = require('should');
var querystring = require('querystring');

var REQUIRED_ITEM = "Digicel Panama"; // TODO mock up this data...
var EMAIL = "example@deginner.com"; // TODO mock up this data...
var PHONE = "50760000001"; // TODO mock up this data...
var mpath = '/order';
var ITEM_ID;
var QUOTE_ID;
var ORDER_ID; // = "ZJG48cahyYXNkDup";
var ORDER_CSRFX; // = "7oSbG5Jz-FCGbP0rCqEtrm900uAFwNwBkxgc";
var PAYMETHOD;
var COOKIE;

describe('OrderController', function() {
  describe('create()', function() {
    it('Create an order', function (done) {
      request(sails.hooks.http.app)
      .get('/item')
      .query({where: JSON.stringify({"name":REQUIRED_ITEM})})
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        var QUOTE_CSRFX = res.header._csrf;
        COOKIE = res.get('set-cookie')[0];
        PAYMETHOD = res.body[0].paymethods[0].id;
        ITEM_ID = res.body[0].id;
        var params = {item: ITEM_ID, _csrf: QUOTE_CSRFX, email: EMAIL,
                      phone: PHONE, paymethod: PAYMETHOD};
        request(sails.hooks.http.app)
        .post('/quote')
        .set('Cookie', COOKIE)
        .send(params)
        .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) {
            done(err);
          }
          QUOTE_ID = res.body.id;
          ORDER_CSRFX = res.header._csrf;
          var params = {item: ITEM_ID, quote: QUOTE_ID, _csrf: ORDER_CSRFX,option: '5'};
          request(sails.hooks.http.app)
          .post(mpath)
          .set('Cookie', COOKIE)
          .send(params)
          .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
          .expect('Content-Type', /json/)
          .expect(200, function(err, res) {
            if (err) {
              done(err);
            }
            ORDER_ID = res.body.id;
            res.body.should.have.property('csrfx');
            res.body['csrfx'].should.equal(ORDER_CSRFX);
            done();
          });
        });
      });
    });
  });
  describe('update()', function() {
    it('Cannot update an order ', function (done) {
      var hackstring = "IhaxURstore";
      request(sails.hooks.http.app)
      .put(mpath + "/" + ORDER_ID)
      .send({'name': hackstring})
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath + "/" + ORDER_ID + "/" + ORDER_CSRFX)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          } else {
            result.body.should.not.have.property('name')
            done();
          }
        });
      });
    });
  });
  describe('destroy()', function() {
    it('Cannot delete an order', function (done) {
      request(sails.hooks.http.app)
      .delete(mpath + "/" + ORDER_ID)
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath + "/" + ORDER_ID + "/" + ORDER_CSRFX)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          } else {
            result.body.should.have.property('csrfx')
            done();
          }
        });
      });
    });
  });
  describe('find()', function() {
    it('Cannot find default list', function (done) {
      request(sails.hooks.http.app)
      .get(mpath)
      .expect(403, done);
    });
    it('Find with id but no csrfx', function (done) {
      request(sails.hooks.http.app)
      .get(mpath + '/' + ORDER_ID)
      .expect(403, done);
    });
    it('Find with id and csrfx', function (done) {
      request(sails.hooks.http.app)
      .get(mpath + '/' + ORDER_ID + "/" + ORDER_CSRFX)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(error, result) {
        if (error) {
          done(error);
        } else {
          result.body.should.have.property['csrfx'];
          done();
        }
      });
    });
  });
});