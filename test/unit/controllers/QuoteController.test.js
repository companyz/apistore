var request = require('supertest');
var should = require('should');
var querystring = require('querystring');

var REQUIRED_ITEM = "Digicel Panama"; // TODO mock up this data...
var EMAIL = "example@deginner.com"; // TODO mock up this data...
var PHONE = "50760000001"; // TODO mock up this data...
var mpath = '/quote';
var QUOTE_ID;
var QUOTE_CSRFX;
var PAYMETHOD;
var COOKIE;

describe('QuoteController', function() {
  describe('create()', function() {
    it('Create a quote', function (done) {
      request(sails.hooks.http.app)
      .get('/item')
      .query({where: JSON.stringify({"name":REQUIRED_ITEM})})
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        QUOTE_CSRFX = res.header._csrf;
        COOKIE = res.get('set-cookie')[0];
        PAYMETHOD = res.body[0].paymethods[0].id
        var params = {item: res.body[0].id, _csrf: QUOTE_CSRFX, email: EMAIL,
                      phone: PHONE, paymethod: PAYMETHOD};
        request(sails.hooks.http.app)
        .post(mpath)
        .set('Cookie', COOKIE)
        .send(params)
        .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) {
            console.log(err);
            done(err);
          }
          QUOTE_ID = res.body.id
          done();
        });
      });
    });
  });
  describe('update()', function() {
    it('Cannot update a quote ', function (done) {
      var hackstring = "IhaxURstore";
      request(sails.hooks.http.app)
      .put(mpath + "/" + QUOTE_ID)
      .send({'name': hackstring})
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath + "/" + QUOTE_ID + "/" + QUOTE_CSRFX)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          }
          result.body.should.not.have.property('name')
          done();
        });
      });
    });
  });
  describe('destroy()', function() {
    it('Cannot delete a quote', function (done) {
      request(sails.hooks.http.app)
      .delete(mpath + "/" + QUOTE_ID)
      .expect(403)
      .end(function(err, res) {
        request(sails.hooks.http.app)
        .get(mpath + "/" + QUOTE_ID + "/" + QUOTE_CSRFX)
        .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(error, result) {
          if (error) {
            done(error);
          }
          result.body.should.have.property('csrfx')
          done();
        });
      });
    });
  });
  describe('find()', function() {
    it('Cannot find default list', function (done) {
      request(sails.hooks.http.app)
      .get(mpath)
      .send()
      .expect(403, done);
    });
    it('Find with id but no csrfx', function (done) {
      request(sails.hooks.http.app)
      .get(mpath + '/' + QUOTE_ID)
      .expect(403, done);
    });
    it('Find with id and csrfx', function (done) {
      request(sails.hooks.http.app)
      .get(mpath + '/' + QUOTE_ID + "/" + QUOTE_CSRFX)
      .expect('_csrf', new RegExp('^.{16,}$')) // _csrf token exists and is > 16 chars
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(error, result) {
        if (error) {
          done(error);
        }
        result.body.should.have.property['csrfx'];
        done();
      });
    });
  });
});