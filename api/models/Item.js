/**
* Item.js
*
* @description :: An item for sale.
*/

module.exports = {
  attributes: {
    category: {
      model: 'Category'
    },
    outlet: {
      model: 'Outlet'
    },
    name: {
      type: "string",
      required: true
    },
    brand: {
      type: "string",
      required: true
    },
    description: {
      type: "string",
      required: true
    },
    external_id: {
      type: "string",
      required: false
    },
    currency: {
      type: "string",
      minLength: 3,
      maxLength: 3,
      enum: ['BTC', 'USD', 'EUR', 'PAB'],
      required: false
    },
    price: {
      type: "number",
      required: false
    },
    icon_url: {
      type: "url",
      required: false
    },
    image_url: {
      type: "url",
      required: false
    },
    required_fields: {
      type: "array",
      required: true
    },
    paymethods: {
      collection: "paymethod",
      via: "items"
    }
  }
};
