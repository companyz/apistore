/**
* Order.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    item: {
      model: "item"
    },
    quote: {
      model: "quote"
    },
    option: {
      type: "string"
    },
    csrfx: {
      type: "string",
      required: true
    },
    address: {
      type: "string",
      maxLength: 35,
      minLength: 26
    },
    satoshis: {
      type: "integer" // TODO is this big enough?
    },
    expires: {
      type: "datetime"
    },
    external_id: {
      type: "string"
    },
    status: {
      type: "string",
      enum: ["pending", "confirming", "complete", "canceled"],
      required: true,
      defaultsTo: "pending"
    }
  }
};

