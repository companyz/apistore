module.exports = function paymethodsHook(sails) {
  return {
    initialize: function(cb) {
      sails.once('hook:orm:loaded', function() {
        Paymethod.findOrCreate({name: "bitcoin"},
                              {name: "bitcoin",
                               display: "Bitcoin",
                               currencies_supported: ['BTC'],
                               image: '/images/bitcoin_accepted_here.png'}).exec(function(err, doc){
                                 return cb();
                               });
      });
    }
  };
};