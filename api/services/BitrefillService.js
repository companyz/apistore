var Bitrefill_client = require('bitrefill');
var bitrefill_client = Bitrefill_client(sails.config.outlet_cfg['bitrefill'])

module.exports = {
  check_order: function(order, cb) {
    bitrefill_client.order_status(order['external_id'], function(err, doc) {
      if (err) {
        cb(err, undefined);
      }
      console.log(doc)
      if (doc['paymentReceived']) {
        order['status'] = "complete";
      }
      cb(undefined, order)
    })
  },
  create_order: function(base, q, cb) {
    bitrefill_client.place_order(q['phone'], q['item']['external_id'], base['option'], q['email'], function(err, doc) {
      if (err) {
        cb(err, undefined);
      }
      if (doc) {
        base['address'] = doc['payment']['address'];
        base['satoshis'] = doc['payment']['satoshiPrice'];
        var expiredt = new Date(parseInt(doc['expirationTime']) * 1000);
        base['expires'] = expiredt;
        base['external_id'] = doc['orderId'];
        cb(err, base);
      }
    });
  },
  create_quote: function(base, cb) {
    bitrefill_client.lookup_number(base['phone'], base['item']['external_id'], function(err, doc) {
      if (err) {
        cb(err, undefined);
      }
      if (doc) {
        console.log(doc);
        base['options'] = {};
        for (var p = 0; p < doc['operator']['packages'].length; p++) {
          base['options'][doc['operator']['packages'][p]['value']] = {
            'satoshis': doc['operator']['packages'][p]['satoshiPrice'],
            'description': doc['operator']['packages'][p]['value'] + " " + doc['operator']['currency'] + " " + doc['operator']['name']
          };
        }
        cb(err, base);
      }
    });
  },
  update_items: function() {
    console.log("updating items...");
    bitrefill_client.inventory(function(err, inv) {
      if (err) {
        return err;
      }
      Category.findOne({'name': "Bill Pay"}).exec(function(err, cat) {
        if (err) {
          return err;
        }
        var catid = cat.id;
        Outlet.findOne({'name': 'Bitrefill'}).exec(function(err, out) {
          if (err) {
            return err;
          }
          var outletid = out.id;
          Paymethod.findOne({'name': 'bitcoin'}).exec(function(err, paym) {
            if (err) {
              return err;
            }
            var paymid = paym.id;
            for (var alpha2 in inv) {
              if (inv.hasOwnProperty(alpha2)) {
                var operators = inv[alpha2]['operators'];
                for (var o in operators) {
                  if (operators.hasOwnProperty(o)) {
                    var iname = operators[o]['name'];
                    var rawitem = {
                      category: catid,
                      outlet: outletid,
                      name: iname,
                      brand: operators[o]['name'],
                      external_id: operators[o]['slug'],
                      description: "Top up your pre-paid phone with " + operators[o]['name'] + ".",
                      icon_url: operators[o]['logoImage'],
                      image_url: operators[o]['logoImage'],
                      required_fields: ['email', 'phone'],
                      paymethods: paymid
                    };
                    Item.findOrCreate({'name': iname, 'outlet': outletid},
                                       rawitem).exec(function(){});
                  }
                };
              }
            }
            console.log("items update complete");
          });
        });
      });
    });
  }
};
