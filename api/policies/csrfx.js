/**
 * CSRFX or Extended CSRF
 *
 * @module      :: Policy
 * @description :: Only allows requests including a unique csrfx token matching the model id
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
  var params = req.allParams();
  if (!(params.hasOwnProperty("id"))) {
    return res.forbidden('Invalid parameters');
  } else if (!(params.hasOwnProperty("csrfx"))) {
    return res.forbidden('Invalid parameters');
  }

  var mod = Quote;
  if (req.options.model == 'order') {
    mod = Order;
  }
  mod.findOne({'csrfx': params.csrfx}, function(err, doc) {
    if (err || doc == undefined) {
      return res.forbidden('Invalid parameters');
    } else {
      next();
    }
  })
};
